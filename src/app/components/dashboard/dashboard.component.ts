import { Component, OnInit } from '@angular/core';

import { StockInterface } from '../../interfaces/stock-interface';
import { StocksService } from '../../services/stocks.service';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

  stocks: Array<StockInterface>;
  symbols: Array<string>;

  constructor(private stocksService: StocksService) { }

  ngOnInit() {
    this.symbols = this.stocksService.get();
    
    this.stocksService.load(this.symbols).subscribe(stocks => {
      this.stocks = stocks;
    })
  }

}
